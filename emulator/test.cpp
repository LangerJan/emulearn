

#include <cstdint>
#include <iostream>

using namespace std;

int main(){

	uint8_t foo[] = {0x2, 0x0, 0x1, 0x0};

	uint16_t &bar = *reinterpret_cast<uint16_t*>(&foo[1]);

	cout << bar << endl;

	return 0;	
}

