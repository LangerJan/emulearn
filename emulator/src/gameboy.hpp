#ifndef __GAMEBOY_HPP__
#define __GAMEBOY_HPP__

#include <array>
#include <cstdint>
#include <memory>
#include <string>
#include <thread>

#include <cpu.hpp>
#include <gpu.hpp>
#include <joypad.hpp>

#include <memory_bus.hpp>
#include <cartridges/basic_cartridge.hpp>

#include <SFML/Graphics.hpp>

class gameboy : public memory_bus{

	private:
		std::shared_ptr<basic_cartridge> m_cart;
		cpu m_cpu;
		joypad m_joypad;
		gpu m_gpu;

		std::thread m_stepper_thread;

		std::shared_ptr<sf::RenderWindow> m_window;

		std::thread m_event_thread;

		std::array<char, 0x1000> m_ram_bank_0;
		std::array<char, 0x1000> m_ram_bank_1;
		std::array<char, 0x7F> m_high_ram;

		gameboy(std::shared_ptr<sf::RenderWindow> window);

	public:
		static std::shared_ptr<gameboy> createGameBoy();
		void loadROM(std::string file);

		uint8_t read8(uint16_t address) override;
		uint16_t read16(uint16_t address) override;

		void write(uint16_t address, uint8_t value) override;
		void write(uint16_t address, uint16_t value) override;
		

		void eventLoop();


};


#endif
