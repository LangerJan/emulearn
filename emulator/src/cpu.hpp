#ifndef __CPU_HPP__
#define __CPU_HPP__


#include <array>
#include <bitset>
#include <cstdint>
#include <functional>
#include <map>
#include <memory>
#include <variant>



#include <memory_bus.hpp>

class cpu{
    
	public:
	struct {
		union {
			struct {
				union{
					uint8_t f;
					struct {
						uint8_t fx : 4;
						uint8_t fc : 1;
						uint8_t fh : 1;
						uint8_t fn : 1;
						uint8_t fz : 1;
					};
				};
				uint8_t a;
			};
			uint16_t af;
		};

		union {
			struct {
				uint8_t c;
				uint8_t b;
			};
			uint16_t bc;
		};

		union {
			struct {
				uint8_t e;
				uint8_t d;
			};
			uint16_t de;
		};

		union {
			struct {
				uint8_t l;
				uint8_t h;
			};
			uint16_t hl;
		};

		uint16_t sp;
		uint16_t pc;

		bool interrupt_enable;
	} m_regs;


	std::shared_ptr<memory_bus> m_mem_bus;

	char m_interrupt_enable;

	bool m_prefix_cb_enabled = false;

	std::map<uint8_t, std::function<void()>> m_operations_table;
	std::map<uint8_t, std::function<void()>> m_cb_operations_table;

	void run();
	void step();


    public:

		cpu();
		void connectBus(std::shared_ptr<memory_bus> mem_bus);

		uint8_t read8(uint16_t address);
		uint16_t read16(uint16_t address);

		uint8_t readIE();
		uint8_t readIF();

	private:

		void fillOperationsMap();

		void setFlag(char flag, bool arg);
		bool getFlag(char flag);

		void execPREFIX_CB();

		int8_t toSigned(uint8_t val);

		/* Loads */
		void execLD(uint8_t& target, uint8_t src);
		void execLD(uint16_t& target, uint16_t src);
		void execLDHL(uint8_t val);

		void execLD_indirect(uint16_t target_address, uint8_t src);
		void execLD_indirect(uint16_t target_address, uint16_t src);

		void execPUSH(uint16_t& reg);
		void execPOP(uint16_t& reg);

		/* ALU */

		void execADD(uint8_t& registerA, uint8_t src, bool with_carry=false);
		void execADD(uint16_t& valA, uint16_t& valB, bool with_carry=false);
		void execADD_SP(uint8_t val);

		void execSUB(uint8_t& registerA, uint8_t src, bool with_carry=false);
		//void execSUB(uint16_t& valA, uint16_t valB, bool with_carry=false);
		
		void execAND(uint8_t val);
		void execOR(uint8_t val);
		void execXOR(uint8_t val);
		void execCP(uint8_t val);

		void execINC(uint8_t& registerA);
		void execINC(uint16_t& target);

		void execDEC(uint8_t& registerA);
		void execDEC(uint16_t& target);
	
		/* Miscellaneous */
		void execSWAP(uint8_t& val);
		void execDAA();
		void execCPL();
		void execCCF();
		void execSCF();
		void execNOP();
		void execHALT();
		void execSTOP();
		void execIE(bool arg);

		void execILLEGAL();


		/* Rotates and Shifts */
		void execRLCA();
		void execRLA();
		void execRRCA();
		void execRRA();

		void execRLC(uint8_t& val);
		void execRL(uint8_t& val);
		void execRRC(uint8_t& val);
		void execRR(uint8_t& val);
		void execSLA(uint8_t& val);
		void execSRA(uint8_t& val);
		void execSRL(uint8_t& val);

		/* Bit operations */
		void execBIT_TEST(uint8_t bitPos, uint8_t val);
		void execBIT_SET(uint8_t bitPos, uint8_t& val);
		void execBIT_RESET(uint8_t bitPos, uint8_t& val);

		/* Jumps */
		void execJP(uint8_t jmpPos, bool cond=true);
		void execJR(uint8_t rel_jmpPos, bool cond=true);

		/* Calls */
		void execCALL(uint16_t callPos, bool cond=true);

		/* Restarts */
		void execRST(uint8_t rst_offset);
		
		/* Returns */
		void execRET(bool cond=true);
		void execRETI();

};


#endif
