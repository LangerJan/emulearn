#include <cpu.hpp>

#include <iostream>

using namespace std;

cpu::cpu()
{
	this->fillOperationsMap();
}

void cpu::connectBus(std::shared_ptr<memory_bus> mem_bus){
	this->m_mem_bus = mem_bus;
}


uint8_t cpu::read8(uint16_t address){
	return this->m_mem_bus->read8(address);
}

uint16_t cpu::read16(uint16_t address){
	return this->m_mem_bus->read16(address);
}

uint8_t cpu::readIE(){
	return 0;
}

uint8_t cpu::readIF(){
	return 0;
}



void cpu::setFlag(char flag, bool arg){
	switch(flag){
		case 'Z': this->m_regs.fz = arg?1:0;break;
		case 'N': this->m_regs.fn = arg?1:0;break;
		case 'H': this->m_regs.fh = arg?1:0;break;
		case 'C': this->m_regs.fc = arg?1:0;break;
		default: throw std::runtime_error("invalid flag set");
	}
}

bool cpu::getFlag(char flag){
	bool result;
	switch(flag){
		case 'Z': result = this->m_regs.fz == 0; break;
		case 'N': result = this->m_regs.fn == 0; break;
		case 'H': result = this->m_regs.fh == 0; break;
		case 'C': result = this->m_regs.fc == 0; break;
		default: throw std::runtime_error("invalid flag set");
	}
	return result;
}


void cpu::run(){
	
	while(true){
		this->step();
	}
}

void cpu::step(){

}

int8_t cpu::toSigned(uint8_t val){
	bool negative = val > 127;

	val = val << 1;
	val = val >> 1;

	int8_t result = static_cast<int8_t>(val);
	return negative?-result:result;
}


void cpu::execILLEGAL(){
	throw std::runtime_error("Illegal opcode called");
}

void cpu::execLD(uint8_t& target, uint8_t src){
	target = src;
}

void cpu::execLD(uint16_t& target, uint16_t src){
	target = src;
}

void cpu::execLDHL(uint8_t val){
	uint16_t address = this->m_regs.sp + this->toSigned(val);
	this->execLD(this->m_regs.hl, address);

	this->m_regs.fz = 0;
	this->m_regs.fn = 0;
}

void cpu::execLD_indirect(uint16_t target_address, uint8_t src){
	this->m_mem_bus->write(target_address, src);
}

void cpu::execLD_indirect(uint16_t target_address, uint16_t src){
	this->m_mem_bus->write(target_address, src);
}

void cpu::execINC(uint8_t& registerA){
	registerA++;
}

void cpu::execINC(uint16_t& target){
	target++;
}

void cpu::execDEC(uint8_t& registerA){
	registerA--;
}

void cpu::execDEC(uint16_t& target){
	target++;
}

void cpu::execADD(uint8_t& registerA, uint8_t src, bool with_carry){
	uint8_t tmp = registerA;
	
	registerA += src + (with_carry?(this->getFlag('C')?1:0):0);

	this->m_regs.fz = (registerA == 0 ? 1:0);
	this->m_regs.fn = 0;
	this->m_regs.fh = (((( (registerA & 0xf) + (src & 0xf) ) & 0x10) == 0x10)? 1:0);
	this->m_regs.fc = (tmp > registerA ? 1:0);
}

void cpu::execADD(uint16_t& val_A, uint16_t& val_B, bool with_carry){
	uint16_t result = val_A + val_B + (with_carry?(this->getFlag('C')?1:0):0);;
	
	this->m_regs.fc = (result > val_A ? 1:0);
	this->m_regs.fh = ( ( ( ((val_A & 0x7ff) + (val_B & 0x7ff)) & 0x800) == 0x800)? 1:0);
	this->m_regs.fn = 0;

	val_B = result;
}

void cpu::execADD_SP(uint8_t val){
	uint16_t val_A = this->m_regs.sp;
	uint16_t val_B = val;

	this->m_regs.sp += val;

	this->m_regs.fz = 0;
	this->m_regs.fn = 0;
	this->m_regs.fh = ( ( ( ((val_A & 0x7ff) + (val_B & 0x7ff)) & 0x800) == 0x800)? 1:0);	
	this->m_regs.fc = (this->m_regs.sp > val_A ? 1:0);
}

void cpu::execSUB(uint8_t& registerA, uint8_t src, bool with_carry){
	uint8_t carry = with_carry?1:0;
	uint8_t result = registerA - src - carry;

	this->m_regs.fz = 0;
	this->m_regs.fn = 1;
	this->m_regs.fh = ((registerA & 0x0f) - (src & 0x0f) - carry) > 127?1:0;	
	this->m_regs.fc = (result > 127 ? 1:0);
}


void cpu::execAND(uint8_t val){
	this->m_regs.a &= val;

	this->m_regs.fz = this->m_regs.a == 0?1:0;
	this->m_regs.fn = 0;
	this->m_regs.fh = 1;
	this->m_regs.fc = 0;
}

void cpu::execXOR(uint8_t val){
	this->m_regs.a ^= val;

	this->m_regs.fz = this->m_regs.a == 0?1:0;
	this->m_regs.fn = 0;
	this->m_regs.fh = 0;
	this->m_regs.fc = 0;
}

void cpu::execOR(uint8_t val){
	this->m_regs.a |= val;

	this->m_regs.fz = this->m_regs.a == 0?1:0;
	this->m_regs.fn = 0;
	this->m_regs.fh = 0;
	this->m_regs.fc = 0;
}

void cpu::execCP(uint8_t val){

}





void cpu::execRET(bool cond){
	if(cond){

	}
}

void cpu::execRETI(){

}

void cpu::execPUSH(uint16_t& reg){
	this->m_regs.sp -= 2;	
	this->m_mem_bus->write(this->m_regs.sp, reg);
}

void cpu::execPOP(uint16_t& reg){
	reg = this->read16(this->m_regs.sp);
	this->m_regs.sp += 2;
}

void cpu::execSWAP(uint8_t& val){
	uint8_t tmp = val >> 4;
	val = (val << 4) | tmp;
}

/*
 * If the least significant four bits of A contain a non-BCD digit 
 * (i. e. it is greater than 9) or the H flag is set, then $06 is 
 * added to the register. Then the four most significant bits are checked. 
 * If this more significant digit also happens to be greater than 
 * 9 or the C flag is set, then $60 is added.
 */
void cpu::execDAA(){

	uint8_t lower4 = this->m_regs.a & 0xF0;
	uint8_t higher4 = (this->m_regs.a & 0x0F) >> 4;

	if(lower4 > 9 || this->m_regs.h){
		this->m_regs.a += 0x06;
	}

	if(higher4 > 9 || this->m_regs.c){
		this->m_regs.a += 0x60;
		
		this->m_regs.fc = 1;
	}
	else
	{
		this->m_regs.fc = 0;
	}

	this->m_regs.fh = 0;
	this->m_regs.fz = this->m_regs.a == 0?1:0;
	
}

void cpu::execCPL(){
	this->m_regs.a = ~this->m_regs.a;
	this->m_regs.fn = 1;
	this->m_regs.fh = 1;
}

void cpu::execCCF(){
	this->m_regs.fc = this->m_regs.fc == 0?1:0;
}

void cpu::execSCF(){
	this->m_regs.fc = 1;
}

void cpu::execNOP(){

}

/*
 * https://www.reddit.com/r/EmuDev/comments/5ie3k7/infinite_loop_trying_to_pass_blarggs_interrupt/
 */ 
void cpu::execHALT(){

}

void cpu::execSTOP(){

}

void cpu::execIE(bool arg){
	this->m_regs.interrupt_enable = arg;
}

void cpu::execJP(uint8_t jmpPos, bool cond){
	if(cond){
		this->m_regs.pc = jmpPos;
	}
}

void cpu::execJR(uint8_t rel_jmpPos, bool cond){
	if(cond){
		int8_t relPos = this->toSigned(rel_jmpPos);
		this->m_regs.pc += relPos;
	}
}

void cpu::execCALL(uint16_t callPos, bool cond){

}

void cpu::execRLCA(){

	uint8_t val = this->m_regs.a;
	val = val >> 7;
	this->m_regs.a = this->m_regs.a << 1;

	this->m_regs.fz = (this->m_regs.a == 0 ? 1:0);
	this->m_regs.fn = 0;
	this->m_regs.fh = 0;
	this->m_regs.fc = (val > 0 ? 1:0);
}

void cpu::execRLA(){

}

void cpu::execRRCA(){

}

void cpu::execRRA(){

}

void cpu::execRLC(uint8_t& val){

}

void cpu::execRL(uint8_t& val){

}
		
void cpu::execRRC(uint8_t& val){

}

void cpu::execRR(uint8_t& val){

}
	
void cpu::execSLA(uint8_t& val){

}

void cpu::execSRA(uint8_t& val){

}

void cpu::execSRL(uint8_t& val){

}

/* Bit operations */
void cpu::execBIT_TEST(uint8_t bitPos, uint8_t val){

}

void cpu::execBIT_SET(uint8_t bitPos, uint8_t& val){

}

void cpu::execBIT_RESET(uint8_t bitPos, uint8_t& val){

}


/* Restarts */
void cpu::execRST(uint8_t rst_offset){
	
}

void cpu::execPREFIX_CB(){
	this->m_prefix_cb_enabled = true;
}


