#ifndef __ROM_HPP__
#define __ROM_HPP__

#include <array>
#include <memory>
#include <string>
#include <fstream>
#include <vector>

class basic_cartridge;

class rom{
	public:	
	/* Header stuff */
	char m_title[16];
	char m_manufacturer_code[4];
	char m_gcb_flag;
	char m_new_licensee_code[2];
	char m_sgb_flag;
	char m_cartridge_type;
	char m_rom_size;
	char m_ram_size;
	char m_destination_code;
	char m_old_licensee_code;
	char m_mask_rom_version_number;
	char m_header_checksum;
	uint16_t m_global_checksum;

	std::vector<std::shared_ptr<std::array<char, 32*1024>>> m_banks;

	std::shared_ptr<std::array<char, 32*1024>> m_bank_zero;
	std::shared_ptr<std::array<char, 32*1024>> m_bank_switched;

	public:
	rom(std::string file);

	int32_t getROMsize();
	int32_t getRAMsize();

	void setBank(uint8_t bankNo);


	std::shared_ptr<basic_cartridge> createCartridge();

};







#endif 
