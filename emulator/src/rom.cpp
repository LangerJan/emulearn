#include "rom.hpp"

#include <iostream>
#include <fstream>
#include <stdexcept>

#include <cartridges/simple_cartridge.hpp>

/**
 * http://gbdev.gg8.se/wiki/articles/The_Cartridge_Header
 */

using namespace std;

rom::rom(std::string filename){
	fstream file (filename, std::fstream::in | std::fstream::binary);
	
	if (file.is_open())
	{
		while(file.good()){
			std::shared_ptr<std::array<char, 32*1024>> bank(new std::array<char, 32*1024>());
			file.read(bank->data(), bank->size());
			this->m_banks.push_back(bank);
		}

		this->m_bank_zero = this->m_banks.at(0);
		this->m_bank_switched = this->m_banks.at(1);		

		copy(&this->m_bank_zero->data()[0x0134], &this->m_bank_zero->data()[0x0143], this->m_title);
		copy(&this->m_bank_zero->data()[0x013F], &this->m_bank_zero->data()[0x0142], this->m_manufacturer_code);
		this->m_gcb_flag = this->m_bank_zero->data()[0x0143];
		copy(&this->m_bank_zero->data()[0x0144], &this->m_bank_zero->data()[0x0145], this->m_new_licensee_code);
		this->m_sgb_flag = this->m_bank_zero->data()[0x0146];
		this->m_cartridge_type = this->m_bank_zero->data()[0x0147];
		this->m_rom_size = this->m_bank_zero->data()[0x0148];
		this->m_ram_size = this->m_bank_zero->data()[0x0149];
		this->m_destination_code = this->m_bank_zero->data()[0x014A];
		this->m_old_licensee_code = this->m_bank_zero->data()[0x014B];
		this->m_mask_rom_version_number = this->m_bank_zero->data()[0x014C];
		this->m_header_checksum = this->m_bank_zero->data()[0x014D];

		this->m_global_checksum = (this->m_bank_zero->data()[0x014F] << 8) + (this->m_bank_zero->data()[0x014E]);
	}
	else
	{
		throw std::runtime_error("Cannot open file");
	}
}

int32_t rom::getROMsize(){
	return 32*1024 << this->m_rom_size;
}

int32_t rom::getRAMsize(){
	switch(this->m_ram_size){
		case 0x00: return 0;
		case 0x01: return 2*1024;
		case 0x02: return 8*1024;
		case 0x03: return 32*1024;
		case 0x04: return 128*1024;
		case 0x05: return 64*1024;
		default: throw std::runtime_error("bad ram size header");
	}
}

void rom::setBank(uint8_t bankNo){
	this->m_bank_switched = this->m_banks.at(bankNo);
}


shared_ptr<basic_cartridge> rom::createCartridge(){
	switch(this->m_cartridge_type){
		case 0x00: return shared_ptr<basic_cartridge>(new simple_cartridge(*this));
		default: throw runtime_error("unsupported cartridge type");
	}
}








