#ifndef __MEMORY_HPP__
#define __MEMORY_HPP__

#include <array>
#include <map>
#include <cstdint>

class memory{
	
	std::map<uint8_t, std::array<uint8_t, 8*1024>> m_switchable_ram_banks;
	std::array<uint8_t, 8*1024> m_video_ram;

	std::map<uint8_t, std::array<uint8_t, 16*1024>> m_switchable_rom_banks;
	std::array<uint8_t, 16*1024> m_rom_bank_0;

};



#endif
