#include <iostream>


#include <gameboy.hpp>
#include <rom.hpp>


#include <SFML/Graphics.hpp>

using namespace std;

gameboy::gameboy(shared_ptr<sf::RenderWindow> window) : 
	m_gpu(window),	
	m_window(window)
{
	this->m_window->setVerticalSyncEnabled(false);
	this->m_window->setActive(false);

}

shared_ptr<gameboy> gameboy::createGameBoy(){
	shared_ptr<sf::RenderWindow> window(new sf::RenderWindow(sf::VideoMode (160, 144), "FOOBAR"));
	shared_ptr<gameboy> result(new gameboy(window));
	
	result->m_cpu.connectBus(result);

	return result;
}



void gameboy::eventLoop(){

	while(this->m_window->isOpen()){
		sf::Event event;
		while (this->m_window->pollEvent (event))
		{
			if(event.type == sf::Event::Closed){
				this->m_window->close();
			}
			else if(event.type == sf::Event::KeyPressed ||	
					event.type == sf::Event::KeyReleased){
				this->m_joypad.updatePad();
			}

		}
		sf::CircleShape shape (100.f);
	  	shape.setFillColor (sf::Color::Green);
		this->m_window->clear();
		this->m_window->draw(shape);
		this->m_window->display();
	}
}

void gameboy::loadROM(std::string file){

	rom r(file);
	this->m_cart = r.createCartridge();
	
	cout << this->m_cart->m_rom.m_title 
		 << " (ROM: " << r.getROMsize() << " bytes) " 
		 << " (RAM: " << r.getRAMsize() << " bytes) " << endl;
}


uint8_t gameboy::read8(uint16_t address){
	if(address <= 0x7FFF){
		// External Cart ROM
		return this->m_cart->read8(address);
	}
	else if(address >= 0x8000 && address <= 0x9FFF){
		// Video RAM
		return this->m_gpu.read8(address);
	}
	else if(address >= 0xA000 && address <= 0xBFFF){
		// External Cart RAM
		return this->m_cart->read8(address);
	}
	else if(address >= 0xC000 && address <= 0xCFFF){
		// Game Boy Internal RAM Bank 0
		address -= 0xC000;
		return this->m_ram_bank_0[address];
	}
	else if(address >= 0xD000 && address <= 0xDFFF){
		// Game Boy Internal RAM Bank 1
		address -= 0xD000;
		return this->m_ram_bank_1[address];
	}
	else if(address >= 0xE000 && address <= 0xEFFF){
		// Game Boy Internal RAM Bank 0
		address -= 0xE000;
		return this->m_ram_bank_0[address];
	}
	else if(address >= 0xF000 && address <= 0xFDFF){
		// Game Boy Internal RAM Bank 1
		address -= 0xF000;
		return this->m_ram_bank_1[address];
	}
	else if(address >= 0xFE00 && address <= 0xFEFF){
		// Sprite Attribute Table
		return this->m_gpu.read8(address);
	}
	else if(address >= 0xFF00 && address <= 0xFF7F){
		// I/O Ports

		switch(address){
			case 0xFF00: return this->m_joypad.readJoyPad();
			default: return 0;
		}

	}
	else if(address >= 0xFF80 && address <= 0xFFFE){
		// High RAM Area
		address -= 0xFF80;
		return this->m_high_ram[address];
	}
	else if(address == 0xFFFF){
		// Interrupt Enable Register
		return this->m_cpu.readIE();
	}
	else {
		cerr << address << endl;
		throw std::runtime_error("Invalid memory call");
	}
}

uint16_t gameboy::read16(uint16_t address){
	uint16_t result;

	uint8_t value_low = this->read8(address+1);
	uint8_t value_high = this->read8(address);

	result = (value_high << 8) | value_low;
	return result;
}

void gameboy::write(uint16_t address, uint8_t value){
	if(address <= 0x7FFF){
		throw std::runtime_error("Illegal write in ROM");
	}
	else if(address >= 0x8000 && address <= 0x9FFF){
		// Video RAM
		this->m_gpu.write(address, value);
	}
	else if(address >= 0xA000 && address <= 0xBFFF){
		this->m_cart->write(address, value);
	}
	else if(address >= 0xC000 && address <= 0xCFFF){
		// Game Boy Internal RAM Bank 0
		address -= 0xC000;
		this->m_ram_bank_0[address] = value;
	}
	else if(address >= 0xD000 && address <= 0xDFFF){
		// Game Boy Internal RAM Bank 1
		address -= 0xD000;
		this->m_ram_bank_1[address] = value;
	}
	else if(address >= 0xE000 && address <= 0xEFFF){
		// Game Boy Internal RAM Bank 0
		address -= 0xE000;
		this->m_ram_bank_0[address] = value;
	}
	else if(address >= 0xF000 && address <= 0xFDFF){
		// Game Boy Internal RAM Bank 1
		address -= 0xF000;
		this->m_ram_bank_1[address] = value;
	}
	else if(address >= 0xFE00 && address <= 0xFEFF){
		// Sprite Attribute Table
		this->m_gpu.write(address, value);
	}
	else if(address >= 0xFF00 && address <= 0xFF7F){
		// I/O Ports

		if(address == 0xFF00){
			this->m_joypad.writeJoyPad(value);
		}


	}
	else if(address >= 0xFF80 && address <= 0xFFFE){
		// High RAM Area
		address -= 0xFF80;
		this->m_high_ram[address] = value;
	}
	else if(address == 0xFFFF){
		// Interrupt Enable Register
		
	}
	else {
		cerr << address << endl;
		throw std::runtime_error("Invalid memory call");
	}
}

void gameboy::write(uint16_t address, uint16_t value){
	uint8_t value_high = (value >> 8);
	uint8_t value_low = (value & 0x00FF);
	this->write(address+1, value_low);
	this->write(address, value_high);
}		





