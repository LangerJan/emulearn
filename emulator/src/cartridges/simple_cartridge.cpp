#include "simple_cartridge.hpp"

#include <iostream>
#include <fstream>
#include <stdexcept>

using namespace std;

simple_cartridge::simple_cartridge(rom r) : basic_cartridge(r){
	r.setBank(1);
}

uint8_t simple_cartridge::read8(uint16_t address){
	
	if(address <= 0x3FFF){
		return m_rom.m_bank_zero->data()[address];
	}
	else if(address >= 0x4000 && address <= 0x7FFF){
		return m_rom.m_bank_switched->data()[address];
	}
	else {
		cerr << "Illegal memory read8: " << address << endl;
		throw runtime_error("illegal memory access on simple catridge");
	}
}

void simple_cartridge::write(uint16_t address, uint8_t){
	cerr << "Illegal memory write8: " << address << endl;
}



