#ifndef __SIMPLE_CATRIDGE_HPP__
#define __SIMPLE_CATRIDGE_HPP__

/**
 * http://gbdev.gg8.se/wiki/articles/The_Cartridge_Header
 */

#include <array>
#include <memory>
#include <string>
#include <fstream>
#include <vector>

#include <rom.hpp>
#include <cartridges/basic_cartridge.hpp>

class simple_cartridge : public basic_cartridge {
	public:	
	simple_cartridge(rom r);

	uint8_t read8(uint16_t address) override;

	void write(uint16_t address, uint8_t value) override;
};

#endif
