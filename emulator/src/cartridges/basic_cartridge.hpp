#ifndef __BASIC_CATRIDGE_HPP__
#define __BASIC_CATRIDGE_HPP__



#include <array>
#include <memory>
#include <string>
#include <fstream>
#include <vector>

#include <rom.hpp>

class basic_cartridge{
	public:
	rom m_rom;
	protected:	
	basic_cartridge(rom r);

	public:
	virtual uint8_t read8(uint16_t address) = 0;

	virtual void write(uint16_t address, uint8_t value) = 0;
};

#endif 
