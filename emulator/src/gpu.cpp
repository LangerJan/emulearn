#include <gpu.hpp>

#include <cstdint>
#include <iostream>
#include <stdexcept>

#include <SFML/Graphics/Image.hpp>

using namespace std;

gpu::gpu(shared_ptr<sf::RenderWindow> window) : 
	m_window(window)
{
	this->m_framebuffer.create(160, 144, sf::Color::Black);
}

void gpu::connectBus(std::shared_ptr<memory_bus> mem_bus){
	this->m_mem_bus = mem_bus;
}


void gpu::triggerRender(){


	uint8_t scrollY = this->m_mem_bus->read8(0xFF42);
	uint8_t scrollX = this->m_mem_bus->read8(0xFF43);

	uint8_t windowY = this->m_mem_bus->read8(0xFF4A);
	uint8_t windowX = this->m_mem_bus->read8(0xFF4B) - 7;
	
	uint16_t background_tilemap_addr;
	if(this->m_regs.tilemap_select == 0){
		background_tilemap_addr = 0x9800;
	}
	else {
		background_tilemap_addr = 0x9C00;
	}

	uint16_t window_tilemap_addr;
	if(this->m_regs.win_tile_map_display_select == 0){
		window_tilemap_addr = 0x9800;
	}
	else {
		window_tilemap_addr = 0x9C00;
	}

	uint16_t tiledata_addr;
	bool treat_tileIdentifier_as_signed = bg_win_tile_data_select == 0;
	if(this->m_regs.bg_win_tile_data_select == 0){
		tiledata_addr = 0x8800;
	}
	else {
		tiledata_addr = 0x8000;
	}
	
}

uint8_t gpu::read8(uint16_t address){

	if(address >= 0x8000 && address <= 0x9FFF){
		address -= 0x8000;
		return this->m_video_ram[address];
	}
	else if(address >= 0xFE00 && address <= 0xFEFF){
		address -= 0xFE00;
		return this->m_sprite_attribute_table[address];
	}
	else
	{
		cerr << address << endl;
		throw std::runtime_error("Invalid memory read call");
	}

}

void gpu::write(uint16_t address, uint8_t value){
	if(address >= 0x8000 && address <= 0x9FFF){
		address -= 0x8000;
		this->m_video_ram[address] = value;
	}
	else if(address >= 0xFE00 && address <= 0xFEFF){
		address -= 0xFE00;
		this->m_sprite_attribute_table[address] = value;
	}
	else
	{
		cerr << address << endl;
		throw std::runtime_error("Invalid memory write call");
	}
}
