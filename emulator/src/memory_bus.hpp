#ifndef __MEMORY_BUS_HPP__
#define __MEMORY_BUS_HPP__

class memory_bus{

	public:
		virtual uint8_t read8(uint16_t address) = 0;
		virtual uint16_t read16(uint16_t address) = 0;

		virtual void write(uint16_t address, uint8_t value) = 0;
		virtual void write(uint16_t address, uint16_t value) = 0;

};


#endif
