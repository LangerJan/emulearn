#include "cpu.hpp"

void cpu::fillOperationsMap(){

	this->m_operations_table = std::map<uint8_t, std::function<void()>>{
		{0x00, std::bind(&cpu::execNOP, this)},
		{0x01, [this](){this->execLD(this->m_regs.bc, this->read16(this->m_regs.pc+1) );}},
		{0x02, [this](){this->execLD_indirect(this->m_regs.bc, this->m_regs.a );}},
		{0x03, [this](){this->execINC(this->m_regs.bc );}},
		{0x04, [this](){this->execINC(this->m_regs.b );}},
		{0x05, [this](){this->execDEC(this->m_regs.b );}},
		{0x06, [this](){this->execLD(this->m_regs.b, this->read8(this->m_regs.pc+1));}},
		{0x07, [this](){this->execRLCA();}},
		{0x08, [this](){this->execLD_indirect(this->read16(this->m_regs.pc+1), this->m_regs.sp );}},
		{0x09, [this](){this->execADD(this->m_regs.hl, this->m_regs.bc);}},
		{0x0a, [this](){this->execLD(this->m_regs.a, this->read8(this->m_regs.bc));}},
		{0x0b, [this](){this->execDEC(this->m_regs.bc );}},
		{0x0c, [this](){this->execINC(this->m_regs.c );}},
		{0x0d, [this](){this->execDEC(this->m_regs.c );}},
		{0x0e, [this](){this->execLD(this->m_regs.c, this->read8(this->m_regs.pc+1));}},
		{0x0f, std::bind(&cpu::execRRCA, this)},

		{0x10, std::bind(&cpu::execSTOP, this)},
		{0x11, [this](){this->execLD(this->m_regs.de, this->read16(this->m_regs.pc+1) );}},
		{0x12, [this](){this->execLD_indirect(this->m_regs.de, this->m_regs.a);}},
		{0x13, [this](){this->execINC(this->m_regs.de );}},
		{0x14, [this](){this->execINC(this->m_regs.d );}},
		{0x15, [this](){this->execDEC(this->m_regs.d );}},
		{0x16, [this](){this->execLD(this->m_regs.d, this->read8(this->m_regs.pc+1));}},
		{0x17, std::bind(&cpu::execRLA, this)},
		{0x18, [this](){this->execJR(this->read8(this->m_regs.pc+1));}},
		{0x19, [this](){this->execADD(this->m_regs.hl, this->m_regs.de);}},
		{0x1a, [this](){this->execLD(this->m_regs.a, this->read8(this->m_regs.de));}},
		{0x1b, [this](){this->execDEC(this->m_regs.de );}},
		{0x1c, [this](){this->execINC(this->m_regs.e );}},
		{0x1d, [this](){this->execDEC(this->m_regs.e );}},
		{0x1e, [this](){this->execLD(this->m_regs.e, this->read8(this->m_regs.pc+1));}},
		{0x1f, std::bind(&cpu::execRRA, this)},

		{0x20, [this](){this->execJR(this->read8(this->m_regs.pc+1), this->m_regs.fz == 0);}},
		{0x21, [this](){this->execLD(this->m_regs.hl, this->read16(this->m_regs.pc+1) );}},
		{0x22, [this](){this->execLD_indirect(this->m_regs.hl, this->m_regs.a);this->m_regs.hl++;}},
		{0x23, [this](){this->execINC(this->m_regs.hl );}},
		{0x24, [this](){this->execINC(this->m_regs.h );}},
		{0x25, [this](){this->execDEC(this->m_regs.h );}},
		{0x26, [this](){this->execLD(this->m_regs.h, this->read8(this->m_regs.pc+1));}},
		{0x27, std::bind(&cpu::execDAA, this)},
		{0x28, [this](){this->execJR(this->read8(this->m_regs.pc+1), this->m_regs.fz == 1);}},
		{0x29, [this](){this->execADD(this->m_regs.hl,this->m_regs.hl);}},
		{0x2a, [this](){this->execLD(this->m_regs.a, this->read8(this->m_regs.hl));this->m_regs.hl++;}},
		{0x2b, [this](){this->execDEC(this->m_regs.hl );}},
		{0x2c, [this](){this->execINC(this->m_regs.l );}},
		{0x2d, [this](){this->execDEC(this->m_regs.l );}},
		{0x2e, [this](){this->execLD(this->m_regs.l, this->read8(this->m_regs.pc+1));}},
		{0x2f, std::bind(&cpu::execCPL, this)},

		{0x30, [this](){this->execJR(this->read8(this->m_regs.pc+1), this->m_regs.fc == 0);}},
		{0x31, [this](){this->execLD(this->m_regs.sp, this->read16(this->m_regs.pc+1) );}},
		{0x32, [this](){this->execLD_indirect(this->m_regs.hl, this->m_regs.a);this->m_regs.hl--;}},
		{0x33, [this](){this->execINC(this->m_regs.sp );}},
		{0x34, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execINC(val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x35, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execDEC(val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x36, [this](){this->execLD_indirect(this->m_regs.hl, this->read8(this->m_regs.pc+1));}},
		{0x37, std::bind(&cpu::execSCF, this)},
		{0x38, [this](){this->execJR(this->read8(this->m_regs.pc+1), this->m_regs.fz == 1);}},
		{0x39, [this](){this->execADD(this->m_regs.hl, this->m_regs.sp);}},
		{0x3a, [this](){this->execLD(this->m_regs.a, this->read8(this->m_regs.hl));this->m_regs.hl--;}},
		{0x3b, [this](){this->execDEC(this->m_regs.sp );}},
		{0x3c, [this](){this->execINC(this->m_regs.a );}},
		{0x3d, [this](){this->execDEC(this->m_regs.a );}},
		{0x3e, [this](){this->execLD(this->m_regs.a, this->read8(this->m_regs.pc+1));}},
		{0x3f, std::bind(&cpu::execCCF, this)},

		{0x40, [this](){this->execLD(this->m_regs.b, this->m_regs.b);}},
		{0x41, [this](){this->execLD(this->m_regs.b, this->m_regs.c);}},
		{0x42, [this](){this->execLD(this->m_regs.b, this->m_regs.d);}},
		{0x43, [this](){this->execLD(this->m_regs.b, this->m_regs.e);}},
		{0x44, [this](){this->execLD(this->m_regs.b, this->m_regs.h);}},
		{0x45, [this](){this->execLD(this->m_regs.b, this->m_regs.l);}},
		{0x46, [this](){this->execLD(this->m_regs.b, this->read8(this->m_regs.hl));}},
		{0x47, [this](){this->execLD(this->m_regs.b, this->m_regs.a);}},
		{0x48, [this](){this->execLD(this->m_regs.c, this->m_regs.b);}},
		{0x49, [this](){this->execLD(this->m_regs.c, this->m_regs.c);}},
		{0x4a, [this](){this->execLD(this->m_regs.c, this->m_regs.d);}},
		{0x4b, [this](){this->execLD(this->m_regs.c, this->m_regs.e);}},
		{0x4c, [this](){this->execLD(this->m_regs.c, this->m_regs.h);}},
		{0x4d, [this](){this->execLD(this->m_regs.c, this->m_regs.l);}},
		{0x4e, [this](){this->execLD(this->m_regs.c, this->read8(this->m_regs.hl));}},
		{0x4f, [this](){this->execLD(this->m_regs.c, this->m_regs.a);}},

		{0x50, [this](){this->execLD(this->m_regs.d, this->m_regs.b);}},
		{0x51, [this](){this->execLD(this->m_regs.d, this->m_regs.c);}},
		{0x52, [this](){this->execLD(this->m_regs.d, this->m_regs.d);}},
		{0x53, [this](){this->execLD(this->m_regs.d, this->m_regs.e);}},
		{0x54, [this](){this->execLD(this->m_regs.d, this->m_regs.h);}},
		{0x55, [this](){this->execLD(this->m_regs.d, this->m_regs.l);}},
		{0x56, [this](){this->execLD(this->m_regs.d, this->read8(this->m_regs.hl));}},
		{0x57, [this](){this->execLD(this->m_regs.d, this->m_regs.a);}},
		{0x58, [this](){this->execLD(this->m_regs.e, this->m_regs.b);}},
		{0x59, [this](){this->execLD(this->m_regs.e, this->m_regs.c);}},
		{0x5a, [this](){this->execLD(this->m_regs.e, this->m_regs.d);}},
		{0x5b, [this](){this->execLD(this->m_regs.e, this->m_regs.e);}},
		{0x5c, [this](){this->execLD(this->m_regs.e, this->m_regs.h);}},
		{0x5d, [this](){this->execLD(this->m_regs.e, this->m_regs.l);}},
		{0x5e, [this](){this->execLD(this->m_regs.e, this->read8(this->m_regs.hl));}},
		{0x5f, [this](){this->execLD(this->m_regs.e, this->m_regs.a);}},

		{0x60, [this](){this->execLD(this->m_regs.h, this->m_regs.b);}},
		{0x61, [this](){this->execLD(this->m_regs.h, this->m_regs.c);}},
		{0x62, [this](){this->execLD(this->m_regs.h, this->m_regs.d);}},
		{0x63, [this](){this->execLD(this->m_regs.h, this->m_regs.e);}},
		{0x64, [this](){this->execLD(this->m_regs.h, this->m_regs.h);}},
		{0x65, [this](){this->execLD(this->m_regs.h, this->m_regs.l);}},
		{0x66, [this](){this->execLD(this->m_regs.h, this->read8(this->m_regs.hl));}},
		{0x67, [this](){this->execLD(this->m_regs.h, this->m_regs.a);}},
		{0x68, [this](){this->execLD(this->m_regs.l, this->m_regs.b);}},
		{0x69, [this](){this->execLD(this->m_regs.l, this->m_regs.c);}},
		{0x6a, [this](){this->execLD(this->m_regs.l, this->m_regs.d);}},
		{0x6b, [this](){this->execLD(this->m_regs.l, this->m_regs.e);}},
		{0x6c, [this](){this->execLD(this->m_regs.l, this->m_regs.h);}},
		{0x6d, [this](){this->execLD(this->m_regs.l, this->m_regs.l);}},
		{0x6e, [this](){this->execLD(this->m_regs.l, this->read8(this->m_regs.hl));}},
		{0x6f, [this](){this->execLD(this->m_regs.l, this->m_regs.a);}},

		{0x70, [this](){this->execLD_indirect(this->m_regs.hl, this->m_regs.b);}},
		{0x71, [this](){this->execLD_indirect(this->m_regs.hl, this->m_regs.c);}},
		{0x72, [this](){this->execLD_indirect(this->m_regs.hl, this->m_regs.d);}},
		{0x73, [this](){this->execLD_indirect(this->m_regs.hl, this->m_regs.e);}},
		{0x74, [this](){this->execLD_indirect(this->m_regs.hl, this->m_regs.h);}},
		{0x75, [this](){this->execLD_indirect(this->m_regs.hl, this->m_regs.l);}},
		{0x76, std::bind(&cpu::execHALT, this)},
		{0x77, [this](){this->execLD_indirect(this->m_regs.hl, this->m_regs.a);}},
		{0x78, [this](){this->execLD(this->m_regs.a, this->m_regs.b);}},
		{0x79, [this](){this->execLD(this->m_regs.a, this->m_regs.c);}},
		{0x7a, [this](){this->execLD(this->m_regs.a, this->m_regs.d);}},
		{0x7b, [this](){this->execLD(this->m_regs.a, this->m_regs.e);}},
		{0x7c, [this](){this->execLD(this->m_regs.a, this->m_regs.h);}},
		{0x7d, [this](){this->execLD(this->m_regs.a, this->m_regs.l);}},
		{0x7e, [this](){this->execLD(this->m_regs.a, this->read8(this->m_regs.hl));}},
		{0x7f, [this](){this->execLD(this->m_regs.a, this->m_regs.a);}},

		{0x80, [this](){this->execADD(this->m_regs.a, this->m_regs.b);}},
		{0x81, [this](){this->execADD(this->m_regs.a, this->m_regs.c);}},
		{0x82, [this](){this->execADD(this->m_regs.a, this->m_regs.d);}},
		{0x83, [this](){this->execADD(this->m_regs.a, this->m_regs.e);}},
		{0x84, [this](){this->execADD(this->m_regs.a, this->m_regs.h);}},
		{0x85, [this](){this->execADD(this->m_regs.a, this->m_regs.l);}},
		{0x86, [this](){this->execADD(this->m_regs.a, this->read8(this->m_regs.hl));}},
		{0x87, [this](){this->execADD(this->m_regs.a, this->m_regs.a);}},
		{0x88, [this](){this->execADD(this->m_regs.a, this->m_regs.b, true);}},
		{0x89, [this](){this->execADD(this->m_regs.a, this->m_regs.c, true);}},
		{0x8a, [this](){this->execADD(this->m_regs.a, this->m_regs.d, true);}},
		{0x8b, [this](){this->execADD(this->m_regs.a, this->m_regs.e, true);}},
		{0x8c, [this](){this->execADD(this->m_regs.a, this->m_regs.h, true);}},
		{0x8d, [this](){this->execADD(this->m_regs.a, this->m_regs.l, true);}},
		{0x8e, [this](){this->execADD(this->m_regs.a, this->read8(this->m_regs.hl), true);}},
		{0x8f, [this](){this->execADD(this->m_regs.a, this->m_regs.a, true);}},

		{0x90, [this](){this->execSUB(this->m_regs.a, this->m_regs.b);}},
		{0x91, [this](){this->execSUB(this->m_regs.a, this->m_regs.c);}},
		{0x92, [this](){this->execSUB(this->m_regs.a, this->m_regs.d);}},
		{0x93, [this](){this->execSUB(this->m_regs.a, this->m_regs.e);}},
		{0x94, [this](){this->execSUB(this->m_regs.a, this->m_regs.h);}},
		{0x95, [this](){this->execSUB(this->m_regs.a, this->m_regs.l);}},
		{0x96, [this](){this->execSUB(this->m_regs.a, this->read8(this->m_regs.hl));}},
		{0x97, [this](){this->execSUB(this->m_regs.a, this->m_regs.a);}},
		{0x98, [this](){this->execSUB(this->m_regs.a, this->m_regs.b, true);}},
		{0x99, [this](){this->execSUB(this->m_regs.a, this->m_regs.c, true);}},
		{0x9a, [this](){this->execSUB(this->m_regs.a, this->m_regs.d, true);}},
		{0x9b, [this](){this->execSUB(this->m_regs.a, this->m_regs.e, true);}},
		{0x9c, [this](){this->execSUB(this->m_regs.a, this->m_regs.h, true);}},
		{0x9d, [this](){this->execSUB(this->m_regs.a, this->m_regs.l, true);}},
		{0x9e, [this](){this->execSUB(this->m_regs.a, this->read8(this->m_regs.hl), true);}},
		{0x9f, [this](){this->execSUB(this->m_regs.a, this->m_regs.a, true);}},

		{0xa0, [this](){this->execAND(this->m_regs.b);}},
		{0xa1, [this](){this->execAND(this->m_regs.c);}},
		{0xa2, [this](){this->execAND(this->m_regs.d);}},
		{0xa3, [this](){this->execAND(this->m_regs.e);}},
		{0xa4, [this](){this->execAND(this->m_regs.h);}},
		{0xa5, [this](){this->execAND(this->m_regs.l);}},
		{0xa6, [this](){this->execAND(this->read8(this->m_regs.hl));}},
		{0xa7, [this](){this->execAND(this->m_regs.a);}},
		{0xa8, [this](){this->execXOR(this->m_regs.b);}},
		{0xa9, [this](){this->execXOR(this->m_regs.c);}},
		{0xaa, [this](){this->execXOR(this->m_regs.d);}},
		{0xab, [this](){this->execXOR(this->m_regs.e);}},
		{0xac, [this](){this->execXOR(this->m_regs.h);}},
		{0xad, [this](){this->execXOR(this->m_regs.l);}},
		{0xae, [this](){this->execXOR(this->read8(this->m_regs.hl));}},
		{0xaf, [this](){this->execXOR(this->m_regs.a);}},

		{0xb0, [this](){this->execOR(this->m_regs.b);}},
		{0xb1, [this](){this->execOR(this->m_regs.c);}},
		{0xb2, [this](){this->execOR(this->m_regs.d);}},
		{0xb3, [this](){this->execOR(this->m_regs.e);}},
		{0xb4, [this](){this->execOR(this->m_regs.h);}},
		{0xb5, [this](){this->execOR(this->m_regs.l);}},
		{0xb6, [this](){this->execOR(this->read8(this->m_regs.hl));}},
		{0xb7, [this](){this->execOR(this->m_regs.a);}},
		{0xb8, [this](){this->execCP(this->m_regs.b);}},
		{0xb9, [this](){this->execCP(this->m_regs.c);}},
		{0xba, [this](){this->execCP(this->m_regs.d);}},
		{0xbb, [this](){this->execCP(this->m_regs.e);}},
		{0xbc, [this](){this->execCP(this->m_regs.h);}},
		{0xbd, [this](){this->execCP(this->m_regs.l);}},
		{0xbe, [this](){this->execCP(this->read8(this->m_regs.hl));}},
		{0xbf, [this](){this->execCP(this->m_regs.a);}},

		{0xc0, [this](){this->execRET(this->m_regs.fz == 0);}},
		{0xc1, [this](){this->execPOP(this->m_regs.bc);}},
		{0xc2, [this](){this->execJP(this->read16(this->m_regs.pc+1), this->m_regs.fz == 0);}},
		{0xc3, [this](){this->execJP(this->read16(this->m_regs.pc+1));}},
		{0xc4, [this](){this->execCALL(this->read16(this->m_regs.pc+1), this->m_regs.fz == 0);}},
		{0xc5, [this](){this->execPUSH(this->m_regs.bc);}},
		{0xc6, [this](){this->execADD(this->m_regs.a, this->read8(this->m_regs.pc+1));}},
		{0xc7, [this](){this->execRST(0x00);}},
		{0xc8, [this](){this->execRET(this->m_regs.fz == 1);}},
		{0xc9, [this](){this->execRET();}},
		{0xca, [this](){this->execJP(this->read16(this->m_regs.pc+1), this->m_regs.fz == 1);}},
		{0xcb, std::bind(&cpu::execPREFIX_CB, this)},
		{0xcc, [this](){this->execCALL(this->read16(this->m_regs.pc+1), this->m_regs.fz == 1);}},
		{0xcd, [this](){this->execCALL(this->read16(this->m_regs.pc+1));}},
		{0xce, [this](){this->execADD(this->m_regs.a, this->read8(this->m_regs.pc+1), true);}},
		{0xcf, [this](){this->execRST(0x08);}},

		{0xd0, [this](){this->execRET(this->m_regs.fc == 0);}},
		{0xd1, [this](){this->execPOP(this->m_regs.de);}},
		{0xd2, [this](){this->execJP(this->read16(this->m_regs.pc+1), this->m_regs.fc == 0);}},
		{0xd3, std::bind(&cpu::execILLEGAL, this)},
		{0xd4, [this](){this->execCALL(this->read16(this->m_regs.pc+1), this->m_regs.fc == 0);}},
		{0xd5, [this](){this->execPUSH(this->m_regs.de);}},
		{0xd6, [this](){this->execSUB(this->m_regs.a, this->read8(this->m_regs.pc+1));}},
		{0xd7, [this](){this->execRST(0x10);}},
		{0xd8, [this](){this->execRET(this->m_regs.fc == 1);}},
		{0xd9, [this](){this->execRETI();}},
		{0xda, [this](){this->execJP(this->read16(this->m_regs.pc+1), this->m_regs.fc == 1);}},
		{0xdb, std::bind(&cpu::execILLEGAL, this)},
		{0xdc, [this](){this->execCALL(this->read16(this->m_regs.pc+1), this->m_regs.fc == 1);}},
		{0xdd, std::bind(&cpu::execILLEGAL, this)},
		{0xde, [this](){this->execSUB(this->m_regs.a, this->read8(this->m_regs.pc+1), true);}},
		{0xdf, [this](){this->execRST(0x18);}},

		{0xe0, [this](){this->execLD_indirect(0xFF00 | this->read8(this->m_regs.pc+1), this->m_regs.a);}},
		{0xe1, [this](){this->execPOP(this->m_regs.hl);}},
		{0xe2, [this](){this->execLD_indirect(0xFF00 | this->m_regs.c, this->m_regs.a);}},
		{0xe3, std::bind(&cpu::execILLEGAL, this)},
		{0xe4, std::bind(&cpu::execILLEGAL, this)},
		{0xe5, [this](){this->execPUSH(this->m_regs.hl);}},
		{0xe6, [this](){this->execAND(this->read8(this->m_regs.pc+1));}},
		{0xe7, [this](){this->execRST(0x20);}},
		{0xe8, [this](){this->execADD_SP(this->read8(this->m_regs.pc+1));}},
		{0xe9, [this](){this->execJP(this->read16(this->m_regs.hl));}},
		{0xea, [this](){this->execLD_indirect(this->read16(this->m_regs.pc+1), this->m_regs.a);}},
		{0xeb, std::bind(&cpu::execILLEGAL, this)},
		{0xec, std::bind(&cpu::execILLEGAL, this)},
		{0xed, std::bind(&cpu::execILLEGAL, this)},
		{0xee, [this](){this->execXOR(this->read8(this->m_regs.pc+1));}},
		{0xef, [this](){this->execRST(0x28);}},

		{0xf0, [this](){this->execLD(this->m_regs.a, this->read8(0xFF00 | this->read8(this->m_regs.pc+1)) );}},
		{0xf1, [this](){this->execPOP(this->m_regs.bc);}},
		{0xf2, [this](){this->execLD(this->m_regs.a, this->read8(0xFF00 | this->m_regs.c) );}},
		{0xf3, [this](){this->execIE(false);}},
		{0xf4, std::bind(&cpu::execILLEGAL, this)},
		{0xf5, [this](){this->execPUSH(this->m_regs.af);}},
		{0xf6, [this](){this->execOR(this->read8(this->m_regs.pc+1));}},
		{0xf7, [this](){this->execRST(0x30);}},
		{0xf8, [this](){this->execLDHL(this->read8(this->m_regs.pc+1));}},
		{0xf9, [this](){this->execLD(this->m_regs.sp, this->m_regs.hl);}},
		{0xfa, [this](){this->execLD(this->m_regs.a, this->read8(this->read16(this->m_regs.pc+1)) );}},
		{0xfb, [this](){this->execIE(true);}},
		{0xfc, std::bind(&cpu::execILLEGAL, this)},
		{0xfd, std::bind(&cpu::execILLEGAL, this)},
		{0xfe, [this](){this->execCP(this->read8(this->m_regs.pc+1));}},
		{0xff, [this](){this->execRST(0x38);}}
	};

	this->m_cb_operations_table = std::map<uint8_t, std::function<void()>>{
		{0x00, [this](){this->execRLC(this->m_regs.b);}},
		{0x01, [this](){this->execRLC(this->m_regs.c);}},
		{0x02, [this](){this->execRLC(this->m_regs.d);}},
		{0x03, [this](){this->execRLC(this->m_regs.e);}},
		{0x04, [this](){this->execRLC(this->m_regs.h);}},
		{0x05, [this](){this->execRLC(this->m_regs.l);}},
		{0x06, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execRLC(val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x07, [this](){this->execRLC(this->m_regs.a);}},
		{0x08, [this](){this->execRRC(this->m_regs.b);}},
		{0x09, [this](){this->execRRC(this->m_regs.c);}},
		{0x0a, [this](){this->execRRC(this->m_regs.d);}},
		{0x0b, [this](){this->execRRC(this->m_regs.e);}},
		{0x0c, [this](){this->execRRC(this->m_regs.h);}},
		{0x0d, [this](){this->execRRC(this->m_regs.l);}},
		{0x0e, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execRRC(val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x0f, [this](){this->execRRC(this->m_regs.a);}},

		{0x10, [this](){this->execRL(this->m_regs.b);}},
		{0x11, [this](){this->execRL(this->m_regs.c);}},
		{0x12, [this](){this->execRL(this->m_regs.d);}},
		{0x13, [this](){this->execRL(this->m_regs.e);}},
		{0x14, [this](){this->execRL(this->m_regs.h);}},
		{0x15, [this](){this->execRL(this->m_regs.l);}},
		{0x16, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execRL(val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x17, [this](){this->execRL(this->m_regs.a);}},
		{0x18, [this](){this->execRR(this->m_regs.b);}},
		{0x19, [this](){this->execRR(this->m_regs.c);}},
		{0x1a, [this](){this->execRR(this->m_regs.d);}},
		{0x1b, [this](){this->execRR(this->m_regs.e);}},
		{0x1c, [this](){this->execRR(this->m_regs.h);}},
		{0x1d, [this](){this->execRR(this->m_regs.l);}},
		{0x1e, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execRR(val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x1f, [this](){this->execRR(this->m_regs.a);}},

		{0x20, [this](){this->execSLA(this->m_regs.b);}},
		{0x21, [this](){this->execSLA(this->m_regs.c);}},
		{0x22, [this](){this->execSLA(this->m_regs.d);}},
		{0x23, [this](){this->execSLA(this->m_regs.e);}},
		{0x24, [this](){this->execSLA(this->m_regs.h);}},
		{0x25, [this](){this->execSLA(this->m_regs.l);}},
		{0x26, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execSLA(val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x27, [this](){this->execSLA(this->m_regs.a);}},
		{0x28, [this](){this->execSRA(this->m_regs.b);}},
		{0x29, [this](){this->execSRA(this->m_regs.c);}},
		{0x2a, [this](){this->execSRA(this->m_regs.d);}},
		{0x2b, [this](){this->execSRA(this->m_regs.e);}},
		{0x2c, [this](){this->execSRA(this->m_regs.h);}},
		{0x2d, [this](){this->execSRA(this->m_regs.l);}},
		{0x2e, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execSRA(val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x2f, [this](){this->execSRA(this->m_regs.a);}},

		{0x30, [this](){this->execSWAP(this->m_regs.b);}},
		{0x31, [this](){this->execSWAP(this->m_regs.c);}},
		{0x32, [this](){this->execSWAP(this->m_regs.d);}},
		{0x33, [this](){this->execSWAP(this->m_regs.e);}},
		{0x34, [this](){this->execSWAP(this->m_regs.h);}},
		{0x35, [this](){this->execSWAP(this->m_regs.l);}},
		{0x36, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execSWAP(val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x37, [this](){this->execSWAP(this->m_regs.a);}},
		{0x38, [this](){this->execSRL(this->m_regs.b);}},
		{0x39, [this](){this->execSRL(this->m_regs.c);}},
		{0x3a, [this](){this->execSRL(this->m_regs.d);}},
		{0x3b, [this](){this->execSRL(this->m_regs.e);}},
		{0x3c, [this](){this->execSRL(this->m_regs.h);}},
		{0x3d, [this](){this->execSRL(this->m_regs.l);}},
		{0x3e, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execSRL(val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x3f, [this](){this->execSRL(this->m_regs.a);}},

		{0x40, [this](){this->execBIT_TEST(0, this->m_regs.b);}},
		{0x41, [this](){this->execBIT_TEST(0, this->m_regs.c);}},
		{0x42, [this](){this->execBIT_TEST(0, this->m_regs.d);}},
		{0x43, [this](){this->execBIT_TEST(0, this->m_regs.e);}},
		{0x44, [this](){this->execBIT_TEST(0, this->m_regs.h);}},
		{0x45, [this](){this->execBIT_TEST(0, this->m_regs.l);}},
		{0x46, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_TEST(0, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x47, [this](){this->execBIT_TEST(0, this->m_regs.a);}},
		{0x48, [this](){this->execBIT_TEST(1, this->m_regs.b);}},
		{0x49, [this](){this->execBIT_TEST(1, this->m_regs.c);}},
		{0x4a, [this](){this->execBIT_TEST(1, this->m_regs.d);}},
		{0x4b, [this](){this->execBIT_TEST(1, this->m_regs.e);}},
		{0x4c, [this](){this->execBIT_TEST(1, this->m_regs.h);}},
		{0x4d, [this](){this->execBIT_TEST(1, this->m_regs.l);}},
		{0x4e, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_TEST(1, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x4f, [this](){this->execBIT_TEST(1, this->m_regs.a);}},

		{0x50, [this](){this->execBIT_TEST(2, this->m_regs.b);}},
		{0x51, [this](){this->execBIT_TEST(2, this->m_regs.c);}},
		{0x52, [this](){this->execBIT_TEST(2, this->m_regs.d);}},
		{0x53, [this](){this->execBIT_TEST(2, this->m_regs.e);}},
		{0x54, [this](){this->execBIT_TEST(2, this->m_regs.h);}},
		{0x55, [this](){this->execBIT_TEST(2, this->m_regs.l);}},
		{0x56, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_TEST(2, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x57, [this](){this->execBIT_TEST(2, this->m_regs.a);}},
		{0x58, [this](){this->execBIT_TEST(3, this->m_regs.b);}},
		{0x59, [this](){this->execBIT_TEST(3, this->m_regs.c);}},
		{0x5a, [this](){this->execBIT_TEST(3, this->m_regs.d);}},
		{0x5b, [this](){this->execBIT_TEST(3, this->m_regs.e);}},
		{0x5c, [this](){this->execBIT_TEST(3, this->m_regs.h);}},
		{0x5d, [this](){this->execBIT_TEST(3, this->m_regs.l);}},
		{0x5e, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_TEST(3, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x5f, [this](){this->execBIT_TEST(3, this->m_regs.a);}},

		{0x60, [this](){this->execBIT_TEST(4, this->m_regs.b);}},
		{0x61, [this](){this->execBIT_TEST(4, this->m_regs.c);}},
		{0x62, [this](){this->execBIT_TEST(4, this->m_regs.d);}},
		{0x63, [this](){this->execBIT_TEST(4, this->m_regs.e);}},
		{0x64, [this](){this->execBIT_TEST(4, this->m_regs.h);}},
		{0x65, [this](){this->execBIT_TEST(4, this->m_regs.l);}},
		{0x66, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_TEST(4, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x67, [this](){this->execBIT_TEST(4, this->m_regs.a);}},
		{0x68, [this](){this->execBIT_TEST(5, this->m_regs.b);}},
		{0x69, [this](){this->execBIT_TEST(5, this->m_regs.c);}},
		{0x6a, [this](){this->execBIT_TEST(5, this->m_regs.d);}},
		{0x6b, [this](){this->execBIT_TEST(5, this->m_regs.e);}},
		{0x6c, [this](){this->execBIT_TEST(5, this->m_regs.h);}},
		{0x6d, [this](){this->execBIT_TEST(5, this->m_regs.l);}},
		{0x6e, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_TEST(5, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x6f, [this](){this->execBIT_TEST(5, this->m_regs.a);}},

		{0x70, [this](){this->execBIT_TEST(6, this->m_regs.b);}},
		{0x71, [this](){this->execBIT_TEST(6, this->m_regs.c);}},
		{0x72, [this](){this->execBIT_TEST(6, this->m_regs.d);}},
		{0x73, [this](){this->execBIT_TEST(6, this->m_regs.e);}},
		{0x74, [this](){this->execBIT_TEST(6, this->m_regs.h);}},
		{0x75, [this](){this->execBIT_TEST(6, this->m_regs.l);}},
		{0x76, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_TEST(6, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x77, [this](){this->execBIT_TEST(6, this->m_regs.a);}},
		{0x78, [this](){this->execBIT_TEST(7, this->m_regs.b);}},
		{0x79, [this](){this->execBIT_TEST(7, this->m_regs.c);}},
		{0x7a, [this](){this->execBIT_TEST(7, this->m_regs.d);}},
		{0x7b, [this](){this->execBIT_TEST(7, this->m_regs.e);}},
		{0x7c, [this](){this->execBIT_TEST(7, this->m_regs.h);}},
		{0x7d, [this](){this->execBIT_TEST(7, this->m_regs.l);}},
		{0x7e, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_TEST(7, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x7f, [this](){this->execBIT_TEST(7, this->m_regs.a);}},

		{0x80, [this](){this->execBIT_RESET(0, this->m_regs.b);}},
		{0x81, [this](){this->execBIT_RESET(0, this->m_regs.c);}},
		{0x82, [this](){this->execBIT_RESET(0, this->m_regs.d);}},
		{0x83, [this](){this->execBIT_RESET(0, this->m_regs.e);}},
		{0x84, [this](){this->execBIT_RESET(0, this->m_regs.h);}},
		{0x85, [this](){this->execBIT_RESET(0, this->m_regs.l);}},
		{0x86, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_RESET(0, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x87, [this](){this->execBIT_RESET(0, this->m_regs.a);}},
		{0x88, [this](){this->execBIT_RESET(1, this->m_regs.b);}},
		{0x89, [this](){this->execBIT_RESET(1, this->m_regs.c);}},
		{0x8a, [this](){this->execBIT_RESET(1, this->m_regs.d);}},
		{0x8b, [this](){this->execBIT_RESET(1, this->m_regs.e);}},
		{0x8c, [this](){this->execBIT_RESET(1, this->m_regs.h);}},
		{0x8d, [this](){this->execBIT_RESET(1, this->m_regs.l);}},
		{0x8e, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_RESET(1, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x8f, [this](){this->execBIT_RESET(1, this->m_regs.a);}},

		{0x90, [this](){this->execBIT_RESET(2, this->m_regs.b);}},
		{0x91, [this](){this->execBIT_RESET(2, this->m_regs.c);}},
		{0x92, [this](){this->execBIT_RESET(2, this->m_regs.d);}},
		{0x93, [this](){this->execBIT_RESET(2, this->m_regs.e);}},
		{0x94, [this](){this->execBIT_RESET(2, this->m_regs.h);}},
		{0x95, [this](){this->execBIT_RESET(2, this->m_regs.l);}},
		{0x96, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_RESET(2, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x97, [this](){this->execBIT_RESET(2, this->m_regs.a);}},
		{0x98, [this](){this->execBIT_RESET(3, this->m_regs.b);}},
		{0x99, [this](){this->execBIT_RESET(3, this->m_regs.c);}},
		{0x9a, [this](){this->execBIT_RESET(3, this->m_regs.d);}},
		{0x9b, [this](){this->execBIT_RESET(3, this->m_regs.e);}},
		{0x9c, [this](){this->execBIT_RESET(3, this->m_regs.h);}},
		{0x9d, [this](){this->execBIT_RESET(3, this->m_regs.l);}},
		{0x9e, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_RESET(3, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0x9f, [this](){this->execBIT_RESET(3, this->m_regs.a);}},

		{0xa0, [this](){this->execBIT_RESET(4, this->m_regs.b);}},
		{0xa1, [this](){this->execBIT_RESET(4, this->m_regs.c);}},
		{0xa2, [this](){this->execBIT_RESET(4, this->m_regs.d);}},
		{0xa3, [this](){this->execBIT_RESET(4, this->m_regs.e);}},
		{0xa4, [this](){this->execBIT_RESET(4, this->m_regs.h);}},
		{0xa5, [this](){this->execBIT_RESET(4, this->m_regs.l);}},
		{0xa6, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_RESET(4, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xa7, [this](){this->execBIT_RESET(4, this->m_regs.a);}},
		{0xa8, [this](){this->execBIT_RESET(5, this->m_regs.b);}},
		{0xa9, [this](){this->execBIT_RESET(5, this->m_regs.c);}},
		{0xaa, [this](){this->execBIT_RESET(5, this->m_regs.d);}},
		{0xab, [this](){this->execBIT_RESET(5, this->m_regs.e);}},
		{0xac, [this](){this->execBIT_RESET(5, this->m_regs.h);}},
		{0xad, [this](){this->execBIT_RESET(5, this->m_regs.l);}},
		{0xae, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_RESET(5, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xaf, [this](){this->execBIT_RESET(5, this->m_regs.a);}},

		{0xb0, [this](){this->execBIT_RESET(6, this->m_regs.b);}},
		{0xb1, [this](){this->execBIT_RESET(6, this->m_regs.c);}},
		{0xb2, [this](){this->execBIT_RESET(6, this->m_regs.d);}},
		{0xb3, [this](){this->execBIT_RESET(6, this->m_regs.e);}},
		{0xb4, [this](){this->execBIT_RESET(6, this->m_regs.h);}},
		{0xb5, [this](){this->execBIT_RESET(6, this->m_regs.l);}},
		{0xb6, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_RESET(6, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xb7, [this](){this->execBIT_RESET(6, this->m_regs.a);}},
		{0xb8, [this](){this->execBIT_RESET(7, this->m_regs.b);}},
		{0xb9, [this](){this->execBIT_RESET(7, this->m_regs.c);}},
		{0xba, [this](){this->execBIT_RESET(7, this->m_regs.d);}},
		{0xbb, [this](){this->execBIT_RESET(7, this->m_regs.e);}},
		{0xbc, [this](){this->execBIT_RESET(7, this->m_regs.h);}},
		{0xbd, [this](){this->execBIT_RESET(7, this->m_regs.l);}},
		{0xbe, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_RESET(7, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xbf, [this](){this->execBIT_RESET(7, this->m_regs.a);}},

		{0xc0, [this](){this->execBIT_SET(0, this->m_regs.b);}},
		{0xc1, [this](){this->execBIT_SET(0, this->m_regs.c);}},
		{0xc2, [this](){this->execBIT_SET(0, this->m_regs.d);}},
		{0xc3, [this](){this->execBIT_SET(0, this->m_regs.e);}},
		{0xc4, [this](){this->execBIT_SET(0, this->m_regs.h);}},
		{0xc5, [this](){this->execBIT_SET(0, this->m_regs.l);}},
		{0xc6, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_SET(0, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xc7, [this](){this->execBIT_SET(0, this->m_regs.a);}},
		{0xc8, [this](){this->execBIT_SET(1, this->m_regs.b);}},
		{0xc9, [this](){this->execBIT_SET(1, this->m_regs.c);}},
		{0xca, [this](){this->execBIT_SET(1, this->m_regs.d);}},
		{0xcb, [this](){this->execBIT_SET(1, this->m_regs.e);}},
		{0xcc, [this](){this->execBIT_SET(1, this->m_regs.h);}},
		{0xcd, [this](){this->execBIT_SET(1, this->m_regs.l);}},
		{0xce, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_SET(1, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xcf, [this](){this->execBIT_SET(1, this->m_regs.a);}},

		{0xd0, [this](){this->execBIT_SET(2, this->m_regs.b);}},
		{0xd1, [this](){this->execBIT_SET(2, this->m_regs.c);}},
		{0xd2, [this](){this->execBIT_SET(2, this->m_regs.d);}},
		{0xd3, [this](){this->execBIT_SET(2, this->m_regs.e);}},
		{0xd4, [this](){this->execBIT_SET(2, this->m_regs.h);}},
		{0xd5, [this](){this->execBIT_SET(2, this->m_regs.l);}},
		{0xd6, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_SET(2, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xd7, [this](){this->execBIT_SET(2, this->m_regs.a);}},
		{0xd8, [this](){this->execBIT_SET(3, this->m_regs.b);}},
		{0xd9, [this](){this->execBIT_SET(3, this->m_regs.c);}},
		{0xda, [this](){this->execBIT_SET(3, this->m_regs.d);}},
		{0xdb, [this](){this->execBIT_SET(3, this->m_regs.e);}},
		{0xdc, [this](){this->execBIT_SET(3, this->m_regs.h);}},
		{0xdd, [this](){this->execBIT_SET(3, this->m_regs.l);}},
		{0xde, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_SET(3, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xdf, [this](){this->execBIT_SET(3, this->m_regs.a);}},

		{0xe0, [this](){this->execBIT_SET(4, this->m_regs.b);}},
		{0xe1, [this](){this->execBIT_SET(4, this->m_regs.c);}},
		{0xe2, [this](){this->execBIT_SET(4, this->m_regs.d);}},
		{0xe3, [this](){this->execBIT_SET(4, this->m_regs.e);}},
		{0xe4, [this](){this->execBIT_SET(4, this->m_regs.h);}},
		{0xe5, [this](){this->execBIT_SET(4, this->m_regs.l);}},
		{0xe6, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_SET(4, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xe7, [this](){this->execBIT_SET(4, this->m_regs.a);}},
		{0xe8, [this](){this->execBIT_SET(5, this->m_regs.b);}},
		{0xe9, [this](){this->execBIT_SET(5, this->m_regs.c);}},
		{0xea, [this](){this->execBIT_SET(5, this->m_regs.d);}},
		{0xeb, [this](){this->execBIT_SET(5, this->m_regs.e);}},
		{0xec, [this](){this->execBIT_SET(5, this->m_regs.h);}},
		{0xed, [this](){this->execBIT_SET(5, this->m_regs.l);}},
		{0xee, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_SET(5, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xef, [this](){this->execBIT_SET(5, this->m_regs.a);}},

		{0xf0, [this](){this->execBIT_SET(6, this->m_regs.b);}},
		{0xf1, [this](){this->execBIT_SET(6, this->m_regs.c);}},
		{0xf2, [this](){this->execBIT_SET(6, this->m_regs.d);}},
		{0xf3, [this](){this->execBIT_SET(6, this->m_regs.e);}},
		{0xf4, [this](){this->execBIT_SET(6, this->m_regs.h);}},
		{0xf5, [this](){this->execBIT_SET(6, this->m_regs.l);}},
		{0xf6, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_SET(6, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xf7, [this](){this->execBIT_SET(6, this->m_regs.a);}},
		{0xf8, [this](){this->execBIT_SET(7, this->m_regs.b);}},
		{0xf9, [this](){this->execBIT_SET(7, this->m_regs.c);}},
		{0xfa, [this](){this->execBIT_SET(7, this->m_regs.d);}},
		{0xfb, [this](){this->execBIT_SET(7, this->m_regs.e);}},
		{0xfc, [this](){this->execBIT_SET(7, this->m_regs.h);}},
		{0xfd, [this](){this->execBIT_SET(7, this->m_regs.l);}},
		{0xfe, [this](){
			uint8_t val = this->m_mem_bus->read8(this->m_regs.hl);
			this->execBIT_SET(7, val);
			this->m_mem_bus->write(this->m_regs.hl, val);
		}},
		{0xff, [this](){this->execBIT_SET(7, this->m_regs.a);}}
	};

}

