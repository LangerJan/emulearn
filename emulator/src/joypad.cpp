#include <joypad.hpp>
#include <SFML/Window/Keyboard.hpp>

#include <bitset>
#include <iostream>

uint8_t joypad::readJoyPad(){
	std::bitset<8> result_bitset(m_joypad);
	
	bool p15_port = result_bitset[5];
	bool p14_port = result_bitset[4];

	result_bitset[3] = (p14_port && this->m_keyDown_pressed)  || (p15_port && this->m_keyStart_pressed);
	result_bitset[2] = (p14_port && this->m_keyUp_pressed)    || (p15_port && this->m_keySelect_pressed);
	result_bitset[1] = (p14_port && this->m_keyLeft_pressed)  || (p15_port && this->m_keyB_pressed);
	result_bitset[0] = (p14_port && this->m_keyRight_pressed) || (p15_port && this->m_keyA_pressed);

	return result_bitset.to_ulong();
}

void joypad::updatePad(){
	this->m_keyLeft_pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Left);
	this->m_keyRight_pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Right);
	this->m_keyUp_pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
	this->m_keyDown_pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Down);

	this->m_keyA_pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::D);
	this->m_keyB_pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::F);
	this->m_keyStart_pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::A);
	this->m_keySelect_pressed = sf::Keyboard::isKeyPressed(sf::Keyboard::D);
}


void joypad::writeJoyPad(uint8_t arg){
	this->m_joypad = arg;
}






