#ifndef __GPU_HPP__
#define __GPU_HPP__

#include <array>
#include <cstdint>
#include <memory>

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Image.hpp>

#include <memory_bus.hpp>

class gpu{

	private:
		std::array<char, 4*1024> m_video_ram;
		std::array<char, 0x100> m_sprite_attribute_table;

		std::shared_ptr<sf::RenderWindow> m_window;

		sf::Image m_framebuffer;

		std::shared_ptr<memory_bus> m_mem_bus;

		struct {
			union {
				struct {
					// Bit 0 - BG Display (for CGB see below) (0=Off, 1=On)
					uint8_t bg_win_display : 1;
					// Bit 1 - OBJ (Sprite) Display Enable (0=Off, 1=On)
					uint8_t obj_display : 1;
					// Bit 2 - OBJ (Sprite) Size (0=8x8, 1=8x16)
					uint8_t obj_size : 1; 
					// Bit 3 - BG Tile Map Display Select (0=9800-9BFF, 1=9C00-9FFF)
					uint8_t tilemap_select : 1; 
					// Bit 4 - BG & Window Tile Data Select (0=8800-97FF, 1=8000-8FFF)
					uint8_t bg_win_tile_data_select : 1;
					// Bit 5 - Window Display Enable (0=Off, 1=On)
					uint8_t win_display : 1;
					// Bit 6 - Window Tile Map Display Select (0=9800-9BFF, 1=9C00-9FFF)
					uint8_t win_tile_map_display_select : 1;
					// Bit 7 - LCD Display Enable (0=Off, 1=On)
					uint8_t lcd_control : 1;
				};
				uint8_t lcdc;
			};
		} m_regs;


	public:



		gpu(std::shared_ptr<sf::RenderWindow> window);
		void connectBus(std::shared_ptr<memory_bus> mem_bus);

		uint8_t read8(uint16_t address);
		void write(uint16_t address, uint8_t value);

		void triggerRender();


};

#endif
