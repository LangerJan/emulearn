#include <iostream>
#include <SFML/Graphics.hpp>

#include "opcodes.hpp"
#include "cartridges/basic_cartridge.hpp"
#include "rom.hpp"
#include "cpu.hpp"

#include <gameboy.hpp>

using namespace std;


int main(int argc, char* argv[])
{
	shared_ptr<gameboy> gb = gameboy::createGameBoy();
	gb->loadROM(argv[1]);

	gb->eventLoop();

/*
	rom r(argv[1]);

	std::shared_ptr<basic_cartridge> cart = r.createCartridge();

	cout << cart->m_rom.m_title << " (ROM: " << r.getROMsize() << " bytes) (RAM: " << r.getRAMsize() << " bytes)" << endl;
	
	foo(std::string(cart->m_rom.m_title));

	cpu gb;
*/

	return 0;
}













