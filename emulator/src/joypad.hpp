#ifndef __JOYPAD_HPP__
#define __JOYPAD_HPP__

#include <atomic>
#include <bitset>
#include <cstdint>
#include <thread>

class joypad{
	private:
		uint8_t m_joypad;	
		std::thread m_event_thread;

		std::atomic<bool> m_keyA_pressed;
		std::atomic<bool> m_keyB_pressed;
		std::atomic<bool> m_keyStart_pressed;
		std::atomic<bool> m_keySelect_pressed;

		std::atomic<bool> m_keyUp_pressed;
		std::atomic<bool> m_keyDown_pressed;
		std::atomic<bool> m_keyLeft_pressed;
		std::atomic<bool> m_keyRight_pressed;

	public:
		uint8_t readJoyPad();
		void writeJoyPad(uint8_t arg);
		void updatePad();


};

#endif
